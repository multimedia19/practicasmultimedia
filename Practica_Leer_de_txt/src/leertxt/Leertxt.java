/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leertxt;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Elias Austria
 */
public class Leertxt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int contador=0;
        int i = 0;
        int RGB[] = {0,0,0};
        int caracter = 0;
        byte c = (byte)255;
        byte y = (byte)0;
        try{
            FileInputStream archivo = new FileInputStream("C:/Users/elias/OneDrive/escritorio/SEPTIMO SEMESTRE/Multimedia/leertxt/prueba1.bmp");
            FileReader txt = new FileReader("C:/Users/elias/OneDrive/escritorio/SEPTIMO SEMESTRE/Multimedia/leertxt/GOT.txt");
            FileOutputStream miFicheroSt = new FileOutputStream( "prucrypt.bmp" );
            BufferedInputStream buff = new BufferedInputStream(archivo);
            DataInputStream datos = new DataInputStream(buff);
            try{
                while (true){
                    byte in = datos.readByte();
                    if(i==3){
                        i=0;
                    }
                    int j= in&0xff;
                    if(contador>=54){
                        System.out.printf("%d - %02X - %d\n", contador++, in,j);
                        while(caracter != -1){
                            caracter = txt.read();
                            miFicheroSt.write((byte)caracter);
                        }
                    }else{
                        System.out.printf("%d - %02X - %d\n", contador++, in,j);
                        miFicheroSt.write(in);
                    }
                }
            }catch(EOFException eof){
                buff.close();
            }
        }catch(IOException e){
            System.out.println("Error  " + e.toString());
        }
        inversa();
        
    }
    
    public static void inversa() {
        int contador=0;
        int i = 0;
        int RGB[] = {0,0,0};
        byte c = (byte)255;
        byte y = (byte)0;
        try{
            FileInputStream archivo = new FileInputStream("prucrypt.bmp");
            FileOutputStream miFicheroSt = new FileOutputStream( "prucrypt.txt" );
            BufferedInputStream buff = new BufferedInputStream(archivo);
            DataInputStream datos = new DataInputStream(buff);
            try{
                while (true){
                    byte in = datos.readByte();
                    if(i==3){
                        i=0;
                    }
                    int j= in&0xff;
                    if(contador>=54){
                        RGB[i] = j;
                        i++;
                        System.out.printf("%d - %02X - %d\n", contador++, in,j);
                        miFicheroSt.write((char)in&0xFF);
                        
                    }else{
                        System.out.printf("%d - %02X - %d\n", contador++, in,j);
                    }
                }
            }catch(EOFException eof){
                    buff.close();
                }
            }catch(IOException e){
                System.out.println("Error  " + e.toString());
            }
        }
    
}

